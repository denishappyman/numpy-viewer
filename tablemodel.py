#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import numpy as np
from PyQt5.QtCore import (Qt, pyqtSignal, pyqtSlot, QAbstractTableModel,
						QModelIndex, QVariant, QRect)
from PyQt5.QtWidgets import (QItemDelegate, QListWidget, QListWidgetItem, QMessageBox)
from PyQt5.QtGui import QBrush
# реализация  класса ComboDelegate дает возможность выбирать значения из списка
class ComboDelegate(QItemDelegate):
	# editorItems=['Combo_Zero', 'Combo_One','Combo_Two']
	editorItems = None
	height = 20
	width = 200

	def createEditor(self, parent, option, index):
		editor = QListWidget(parent)
		# выясняем значение ячейки на которую кликнул пользователь
		value = str(index.model().data(index, Qt.DisplayRole).value())
		self.editorItems = list(map(str,range(1,6)))
		# и если его нет в списке,то добавляем его  по индексу 0
		# это нужно для корректной обработки ввода (отмены ввода) данных
		if value not in self.editorItems:
			self.editorItems.insert(0, value)

		editor.itemActivated.connect(self.itemActivated)
		return editor

	def setEditorData(self, editor, index):
		# заполняем QListWidget элементами
		z = 0
		for item in self.editorItems:
			ai = QListWidgetItem(item)
			ai.setTextAlignment(Qt.AlignRight)
			editor.addItem(ai)
			if item == index.data():
				editor.setCurrentItem(editor.item(z))
			z += 1

	def updateEditorGeometry(self, editor, option, index):
		# определяем координаты для QListWidget
		left = option.rect.left()
		top = option.rect.top()
		width = option.rect.width()
		editor_rect = QRect(left, top, width, self.height * len(self.editorItems))
		editor.setGeometry(editor_rect)

	def setModelData(self, editor, model, index):
		editorIndex=editor.currentIndex()
		text=editor.currentItem().text() 
		model.setData(index, text, Qt.EditRole)
		self.closeEditor.emit(self.sender())
		# print '\t\t\t ...setModelData() 1', text

	@pyqtSlot()
	def itemActivated(self): 
		self.commitData.emit(self.sender())

# переопределяем нужные методы класса QAbstractTableModel для реализации
# изменения и пересчета данных в таблице, как указано в тех.задании
class TableModel(QAbstractTableModel):
	#сигнал, изменилось значение первой колонки ( с индексом 0 )
	dataChangedInColumn0 = pyqtSignal(int)
	#сигнал, изменилось значение второй колонки ( с индексом 1 )
	dataChangedInColumn1 = pyqtSignal(int)
	#сигнал, изменилось значение третьей колонки ( с индексом 2 )
	dataChangedInColumn2 = pyqtSignal()	

	def __init__(self, dataIn, parent=None, *args):
		QAbstractTableModel.__init__(self, parent, *args)

		# массив для хранения данных
		self.items = dataIn

		# exception message
		self.msg = QMessageBox()     
		self.msg.setIcon(QMessageBox.Critical)
		self.msg.setWindowTitle("Error")

		# cоединяем сигналы и слоты
		self.dataChangedInColumn0.connect(self.onDataChangeInColumn0)
		self.dataChangedInColumn1.connect(self.onDataChangeInColumn1)
		self.dataChangedInColumn2.connect(self.onDataChangeInColumn2)		

	# возвращаем количество строк в TableModel
	def rowCount(self, parent=QModelIndex()):
		try:
			return len(self.items)
		except:
			print("Need an array with items")
			return 0

	# возвращаем количество столбцов в TableModel
	def columnCount(self, parent=QModelIndex()):
		try:
			return len(self.items[0])
		except:
			print("Need an array with items")
			return 0

	# метод для доступа к данным модели
	def data(self, index, role):
		if not index.isValid():
			return QVariant()

		# выясняем значение ячейки
		value = self.items[index.row()][index.column()]
			# изменение фона ячейки предусмотрено только для колонки с индексом й
		if role == Qt.BackgroundRole and index.column() == 1:
			if value > 0:
				# значение в ячейке положительное - background в зеленый цвет
				return QBrush(Qt.green)
			elif value < 0:
				 # отрицательное - background в красный цвет
				return QBrush(Qt.red)
			else:
				# ячейку со значением 0 не закрашиваем
				return QBrush(Qt.white)
		elif role == Qt.TextAlignmentRole:
			return Qt.AlignRight

		row=index.row()
		if row>len(self.items): 
			return QVariant()

		item=str(value)
		if role == Qt.DisplayRole:
			return QVariant(item) 

	def flags(self, index):
		assert (len(self.items[0]) > 0), "Need an array with columns"
		if index.column() == 0:
			# для колонки с индексом 0 разрешаем выделение и редактирование ячеек
			return Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled
		else:
			# для остальных колонок разрешаем выделение ячеек
			return Qt.ItemIsSelectable | Qt.ItemIsEnabled

	def setData(self, index, text, role):
		# эмит сигнала dataChanged произойдет если данные изменились,
		# для этого сравниваем что пришло от делегата и то что было до этого
		if (str(self.items[index.row()][index.column()]) != str(text) and		
		index.isValid() and role == Qt.EditRole):

			self.items[index.row()][index.column()]=str(text)
			self.dataChanged.emit(index, index);
			if index.column() == 0:
				# эмит сигнала по изменению данных в колонке с индексом 0
				self.dataChangedInColumn0.emit(index.row())
			# print(self.items) # delete before release
			return True
		else:
			return False

	def updateModel(self, dataIn):
		# TODO: количество столбцов должно быть 3
		if len(dataIn[0]) == 3:
			print ("Updating Model")
			self.beginResetModel()
			self.items = dataIn
			self.endResetModel()
			self.layoutChanged.emit()
		else:
			self.msg.setText("Допускается массив с 3 столбцами.")
			self.msg.exec_()			

	@pyqtSlot(int)
	def onDataChangeInColumn0(self,row):
		# print("onDataChangeInColumn0 invoked. Need to recalculate column2")
		# ячейка второго столбца принимает тоже самое значение что и первого
		assert (len(self.items[0]) == 3), "Need an array with 3 columns"
		self.items[row][1]=self.items[row][0]
		# здесь эмит сигнала, что значение во втором столбце изменилось
		self.dataChangedInColumn1.emit(row)

	@pyqtSlot(int)
	def onDataChangeInColumn1(self,row):
		# print("onDataChangeInColumn1 invoked. Need to recalculate column3")
		self.recalculateCumSum()

	@pyqtSlot()
	def onDataChangeInColumn2(self):
		print("onDataChangeInColumn2 invoked")

	def recalculateCumSum(self):
		# проверяем, что массив двухмерный, три столбца
		assert (len(self.items[0]) == 3), "Need an array with 3 columns"
		# column = len(self.items[0]) - 1
		column = 2
		# TODO не все а начиная с изменившейся строки и до конца
		self.items[:, column] = np.cumsum(self.items[:, column - 1], axis = 0)
		self.dataChangedInColumn2.emit()
		self.layoutChanged.emit()
		# print("recalculateCumSum invoked")


if __name__ == '__main__':
	app = QApplication(sys.argv)
	model = TableModel(None)
	model.updateModel(np.array([[0, 1, 1], [1, 1, 2], [2, 1, 3], [2, 2, 3]], dtype=np.int32))    
	tableView = QTableView()
	tableView.setModel(model)
	delegate = ComboDelegate()
	tableView.setItemDelegateForColumn(0, delegate)
	tableView.show()
	sys.exit(app.exec_())