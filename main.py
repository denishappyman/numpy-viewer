#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import numpy as np
import pyqtgraph as pg
import h5py

from PyQt5.QtWidgets import (QApplication, QMainWindow, QGridLayout, 
	QWidget, QPushButton, QItemDelegate, QListWidget, QDesktopWidget,
	QListWidgetItem, QFileDialog, QTableView)
from PyQt5.QtCore import Qt
from tablemodel import ComboDelegate, TableModel

class MainWidget(QWidget):
	grid_layout = None
	model = None
	table = None
	delegate = None
	loadButton = None
	saveButton = None
	createNpSampleButton = None
	recalculateCumSumButton = None
	plotw = None

	def __init__(self):
		super().__init__()
		self.initUI()

	def initUI(self):
		# создаём сетку
		self.grid_layout = QGridLayout()
		# устанавливаем данное размещение в центральный виджет
		self.setLayout(self.grid_layout)

		# создаем модель таблицы
		self.model = TableModel(None)
		# заполняем модель
		# self.createNpSample()

		# создаем view таблицы
		self.table = QTableView()
		# соединяем model и view			
		self.table.setModel(self.model)		

		# создаем класс для вывода выпадающего
		# списка по нажанию на любую ячейку колонки с индексом 0
		delegate = ComboDelegate()											
		self.table.setItemDelegateForColumn(0, delegate)

		# делаем ресайз колонок по содержимому
		self.table.resizeColumnsToContents()
		
		# создаем кнопки
		self.loadButton = QPushButton("Load")							
		self.saveButton = QPushButton("Save")
		# отключаем кнопку Save, т.к. сохранять еще нечего 
		self.saveButton.setEnabled(False)
		# TODO: добавить подписи осей из столбцов и название графика
		# создаем виджет графика
		self.plotw = pg.PlotWidget()

		# отображаем сетку на графике
		self.plotw.showGrid(x=True, y=True)								

		# добавляем кнопки в сетку
		self.grid_layout.addWidget(self.loadButton, 0, 0)
		# TODO: saveButton is disabled if no data loaded
		self.grid_layout.addWidget(self.saveButton, 1, 0)
		# добавляем таблицу в сетку
		self.grid_layout.addWidget(self.table, 0, 1, 25, 1)				
		# добавляем график в сетку
		self.grid_layout.addWidget(self.plotw, 0, 2, 25, 2)			

		# cоединяем сигналы и слоты
		self.loadButton.clicked.connect(self.loadFile)
		self.saveButton.clicked.connect(self.saveFile)
		self.model.layoutChanged.connect(self.enableSaveButton)		
		self.table.selectionModel().selectionChanged.connect(self.onSelectionChange)		

		# self.setGeometry(300, 300, 300, 150)
		self.setWindowTitle('Numpy viewer')
		self.center()
		self.show()

	# обработка вывода на данных график при изменении выделения ячеек 
	# в table(QTableView)
	def onSelectionChange(self):
		# получаем список всех полностью выделенных столбцов
		selected_columns = self.table.selectionModel().selectedColumns()
		if len(selected_columns) == 2:
			# имеем два полностью выделенных столбца 
			# и выводим график их зависимости из модели
			self.plotw.plot(x = self.model.items[:, selected_columns[0].column()],
							y = self.model.items[:, selected_columns[1].column()],
							symbol='o')
		else:
			# если график что-то уже нарисовал, то очистить содержимое			
			if len(self.plotw.listDataItems()) > 0:
				self.plotw.clear()

	def loadFile(self):   
		options = QFileDialog.Options()
		# options |= QFileDialog.DontUseNativeDialog
		fileName, _ = QFileDialog.getOpenFileName(self,"Open file","",
											"Text Files (*.txt);;h5py Files (*.hdf)", 
											 options=options)
		if fileName:
			# извлекаем расширение выбранного пользователем файла
			# проверяем в зависимости от типа файла меняем логику
			try: 
				name, ext = os.path.splitext(fileName)
				# assert (ext != ""), "Unknown file extension!"
				if (name == "" or ext == ""): raise ValueError ('Ошибка именования файла.')
			except:
				self.model.msg.setText("Ошибка именования файла.")
				self.model.msg.exec_()
				return

			if ext == ".hdf":
				print(".hdf extension selected")
				try:
					with h5py.File(fileName, 'r') as hf:
						# заполняем массив в таблицу из h5py файла (датасет с индексом 0)
						self.model.updateModel(hf[list(hf.keys())[0]][:])
				except:
					# TODO: Вынести в отдельную функцию (DRY)					
					self.model.msg.setText("Ошибка открытия файла.")
					self.model.msg.exec_()		
			else:
				print("TODO: .txt extension selected")
				try:
					# заполняем массив в таблицу из текстового файла
					self.model.updateModel(np.loadtxt(	fname = fileName, 
														dtype = int))
				except:
					# TODO: Вынести в отдельную функцию (DRY)					
					self.model.msg.setText("Ошибка открытия файла.")
					self.model.msg.exec_()
			self.table.resizeColumnsToContents()

	def saveFile(self):   
		options = QFileDialog.Options()
		# options |= QFileDialog.DontUseNativeDialog
		fileName, _ = QFileDialog.getSaveFileName(self,"Save file", "",
											"Text Files (*.txt);;h5py Files (*.hdf)", 
											options=options)
		if fileName:
			# проверяем в зависимости от типа файла меняем логику
			# извлекаем расширение выбранного пользователем файла
			try: 
				name, ext = os.path.splitext(fileName)
				# assert (ext != ""), "Unknown file extension!"
				if (name == "" or ext == ""): raise ValueError ('Ошибка именования файла.')
			except:
				self.model.msg.setText("Ошибка именования файла.")
				self.model.msg.exec_()
				return

			if ext == ".hdf":
				print(".hdf extension selected")
				# TODO: try
				try:
					with h5py.File(fileName, 'w') as hf:
						 # сохраняем таблицу в датасет h5py файла (датасет с индексом 0)
						 hf.create_dataset("dataset0",  data = self.model.items)
				except:
					# TODO: Вынести в отдельную функцию (DRY)					
					self.model.msg.setText("Ошибка cохранения файла.")
					self.model.msg.exec_()					   
			else:
				# сохраняем таблицу в текстовый файл (human readable)
				try:
					print("TODO: .txt extension selected")
					np.savetxt(	fname 		= fileName, 
								X 			= self.model.items, 
								fmt			= '%d')
				except:
					# TODO: Вынести в отдельную функцию (DRY)
					self.model.msg.setText("Ошибка cохранения файла.")
					self.model.msg.exec_()						  


	def enableSaveButton(self):
		# активируем кнопку Save после того как в таблицу загрузились хоть
		# какие то данные по сигналу model.layoutChanged().emit
		if not self.saveButton.isEnabled():
			self.saveButton.setEnabled(True)

	# создаем непустой массив
	def createNpSample(self):
		a = np.array(  [[-1, -1, -1], 
						[-1, -1, -2],  
						[ 2,  2,  0], 
						[ 2,  2,  2]], 
						dtype=np.int32)

		# этот массив побольше для тестирования
		# a = np.random.randint(-2, 2, size=(100,3), dtype='int32')

		# отправляем массив в модель
		self.model.updateModel(a)

	def center(self):
		# геометрия виджета
		qr = self.frameGeometry()

		# получаем центральную точку экрана
		cp = QDesktopWidget().availableGeometry().center()

		# перемещаем центр прямоугольника в центр экрана
		qr.moveCenter(cp)

		self.move(qr.topLeft())			

if __name__ == '__main__':
	app = QApplication(sys.argv)
	mainWidget = MainWidget()
	sys.exit(app.exec_())